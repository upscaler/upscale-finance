define(['Q', 'underscore', 'mifosX'], function (Q) {
    var components = {
        models: [
            'models.9e2cb9ff'
        ],
        services: [
            'ResourceFactoryProvider',
            'HttpServiceProvider',
            'AuthenticationService',
            'SessionManager',
            'Paginator',
            'UIConfigService'
        ],
        controllers: [
            'controllers.67bbdcc2'
        ],
        filters: [
            'filters.fa2ac2a1'
        ],
        directives: [
            'directives.e6b3282c'
        ]
    };

    return function() {
        var defer = Q.defer();
        require(_.reduce(_.keys(components), function (list, group) {
            return list.concat(_.map(components[group], function (name) {
                return group + "/" + name;
            }));
        }, [
            'routes-initialTasks-webstorage-configuration.81248143'
        ]), function(){
            defer.resolve();
        });
        return defer.promise;
    }
});
